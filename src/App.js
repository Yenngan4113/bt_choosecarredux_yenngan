import logo from "./logo.svg";
import "./App.css";
import { Fragment } from "react";
import ChooseCar from "./Component/ChooseCar";

function App() {
  return (
    <Fragment>
      <ChooseCar />
    </Fragment>
  );
}

export default App;
