import React, { Component } from "react";
import { connect } from "react-redux";

class ChooseCar extends Component {
  render() {
    return (
      <div>
        <div className="row align-items-center">
          <div className="col-6">
            <img src={this.props.url} style={{ width: "100%" }} />
          </div>
          <div className="col-4">
            <button
              className="btn btn-danger mx-3 px-3"
              onClick={this.props.handleRedCar}
            >
              RED
            </button>
            <button
              className="btn btn-secondary mx-3 px-3"
              onClick={this.props.handleSilverCar}
            >
              SILVER
            </button>
            <button
              className="btn btn-dark mx-3 px-3"
              onClick={this.props.handleBlackCar}
            >
              BLACK
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { url: state.url };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleRedCar: () => {
      dispatch({
        type: "Red",
      });
    },
    handleSilverCar: () => {
      dispatch({
        type: "Silver",
      });
    },
    handleBlackCar: () => {
      dispatch({
        type: "Black",
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ChooseCar);
