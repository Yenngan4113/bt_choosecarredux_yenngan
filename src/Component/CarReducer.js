let initialState = {
  url: "./img/car/black-car.jpg",
};

export const CarReducer = (state = initialState, action) => {
  switch (action.type) {
    case "Red": {
      return { ...state, url: "./img/car/red-car.jpg" };
    }
    case "Silver": {
      return { ...state, url: "./img/car/silver-car.jpg" };
    }
    case "Black": {
      return { ...state, url: "./img/car/black-car.jpg" };
    }
    default:
      return state;
  }
};
